import datetime

from fastapi.testclient import TestClient
from main import app

client = TestClient(app)


def setup():
    client.delete("/client/999999")
    client.delete("/mailing/999999")


def test_add_client():
    data = {
        "id": 999999,
        "phone_number": "79501234567",
        "operator_code": 40,
        "tag": "tag_test",
        "timezone": "EU"
    }
    response = client.post("/client", json=data)
    assert response.status_code == 200
    assert response.json() == {"status": "Success"}


def test_add_exist_client():
    data = {
        "id": 999999,
        "phone_number": "79501234567",
        "operator_code": 40,
        "tag": "tag_test",
        "timezone": "EU"
    }
    response = client.post("/client", json=data)
    assert response.status_code == 400
    assert response.json() == {"detail": "Client already exists"}


def test_add_mailing():
    tomorrow = datetime.datetime.now() + datetime.timedelta(days=1)
    data = {
        "id": 999999,
        "start_date_time": str(int(datetime.datetime.now().timestamp())),
        "end_date_time": str(int(tomorrow.timestamp())),
        "text": "tag_test",
        "filter": "40"
    }
    response = client.post("/mailing", json=data)
    assert response.status_code == 200
    assert response.json() == {"status": "Success"}


def test_add_exist_mailing():
    tomorrow = datetime.datetime.now() + datetime.timedelta(days=1)
    data = {
        "id": 999999,
        "start_date_time": str(int(datetime.datetime.now().timestamp())),
        "end_date_time": str(int(tomorrow.timestamp())),
        "text": "tag_test",
        "filter": "40"
    }
    response = client.post("/mailing", json=data)
    assert response.status_code == 400
    assert response.json() == {"detail": "Mailing already exists"}


def test_stats_mailing():
    response = client.get("/mailing/statistic/999999")
    assert response.status_code == 200
    assert response.headers.get('Content-Type') == "application/json"


def test_status_stats_mailing():
    response = client.get("/mailing/statistic/status")
    assert response.status_code == 200
    assert response.headers.get('Content-Type') == "application/json"


def test_delete_client():
    response = client.delete("/client/999999")
    assert response.status_code == 200
    assert response.headers.get('Content-Type') == "application/json"


def test_delete_mailing():
    response = client.delete("/mailing/999999")
    assert response.status_code == 200
    assert response.headers.get('Content-Type') == "application/json"


def teardown():
    client.delete("/client/999999")
    client.delete("/mailing/999999")
