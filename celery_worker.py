from celery import Celery
from celery.utils.log import get_task_logger

from database import Message as DbMessage
from database import SessionLocal

import json
import os
import datetime
import requests
import concurrent.futures

# Настройка Celery для подключение к RabbitMQ
celery = Celery('tasks', broker=os.environ.get("CELERY_BROKER_URL", "amqp://guest:guest@127.0.0.1:5672//"))
celery_log = get_task_logger(__name__)

# Адрес внешнего сервера
URL = "https://probe.fbrq.cloud/v1/send/"
# Добавление JWT токена в headers
head = {'Authorization': 'Bearer {}'.format(os.environ.get("AUTHORIZATION_TOKEN"))}


# Функция отправления запроса на внешний сервис
def send_request(client):
    data = {
        "id": client[2],
        "phone": client[3],
        "text": client[1]
    }
    try:
        response = requests.post(URL + str(data["id"]), json=data, headers=head, timeout=30)
    except Exception:
        status_code = 2
    else:
        status_code = json.loads(response.text)["code"]
    db = SessionLocal()
    new_message = DbMessage(
        create_date_time=datetime.datetime.now(),
        status=status_code,
        mailing_id=client[0],
        client_id=client[2],
    )
    db.add(new_message)
    db.commit()
    db.refresh(new_message)


# Создание задачи для Celery
@celery.task
def send_message(client_list):
    with concurrent.futures.ThreadPoolExecutor() as executor:
        res = [executor.submit(send_request, client) for client in client_list]
        concurrent.futures.wait(res)
