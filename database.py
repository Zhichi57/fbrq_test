from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, ForeignKey, Integer, String, DateTime

import os

# Получение переменных из окружения
POSTGRES_USER = os.getenv("POSTGRES_USER")
POSTGRES_DB = os.getenv("POSTGRES_DB")
POSTGRES_PASSWORD = os.getenv("POSTGRES_PASSWORD")

# Формирование адреса для подключения к базе данных
SQLALCHEMY_DATABASE_URL = "postgresql://{user}:{password}@db/{db}".format(user=POSTGRES_USER,
                                                                          password=POSTGRES_PASSWORD, db=POSTGRES_DB)
# Подключение к базе данных
engine = create_engine(SQLALCHEMY_DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


# Сущность "клиент"
class Client(Base):
    __tablename__ = "clients"

    id = Column(Integer, primary_key=True, index=True)
    phone_number = Column(String, unique=False)
    operator_code = Column(String, unique=False)
    tag = Column(String, unique=False, index=True)
    timezone = Column(String, unique=False)


# Сущность "рассылка"
class Mailing(Base):
    __tablename__ = "mailing"

    id = Column(Integer, primary_key=True, index=True)
    start_date_time = Column(DateTime, unique=False)
    end_date_time = Column(DateTime, unique=False)
    text = Column(String, unique=False)
    filter = Column(String, unique=False)


# Сущность "сообщение"
class Message(Base):
    __tablename__ = "messages"

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    create_date_time = Column(DateTime, unique=False)
    status = Column(Integer, unique=False)
    mailing_id = Column(Integer, ForeignKey("mailing.id", onupdate="CASCADE", ondelete="SET NULL"))
    client_id = Column(Integer, ForeignKey("clients.id", onupdate="CASCADE", ondelete="SET NULL"))
