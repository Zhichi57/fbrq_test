from fastapi import FastAPI, HTTPException
from fastapi.responses import JSONResponse
from fastapi.openapi.utils import get_openapi
from pydantic import BaseModel, validator
from database import Base, engine, SessionLocal
from database import Client as DbClient
from database import Mailing as DbMailing
from database import Message as DbMessage
from sqlalchemy import delete, or_, exc, func
from celery_worker import send_message

import datetime
import re

Base.metadata.create_all(bind=engine)

app = FastAPI()


# Добавление своей информации в документацию
def my_schema():
    openapi_schema = get_openapi(
        title="Сервис управления рассылками API администрирования и получения статистики.",
        version="1.0",
        routes=app.routes,

    )
    openapi_schema["info"] = {
        "title": "Сервис управления рассылками API администрирования и получения статистики.",
        "version": "1.0",
        "contact": {
            "name": "Ilya Alekhin",
            "email": "ilya22121997@yandex.ru"
        },
    }
    app.openapi_schema = openapi_schema
    return app.openapi_schema


app.openapi = my_schema


# Модель для сущности "Клиент"
class Client(BaseModel):
    id: int
    phone_number: str
    operator_code: str
    tag: str
    timezone: str

    # Проверка формата данных для поля телефон
    @validator("phone_number")
    def check_phone_number(cls, v):
        reg = r"7\d{10}"
        if not re.match(reg, v):
            raise HTTPException(status_code=400, detail="Wrong phone number format")
        return v


# Модель для сущности "Сообщение"
class Message(BaseModel):
    id: int
    create_date_time: datetime.datetime
    status: int
    mailing_id: int
    client_id: int


# Модель для сущности "Рассылка"
class Mailing(BaseModel):
    id: int
    start_date_time: datetime.datetime
    end_date_time: datetime.datetime
    text: str
    filter: str

    # Проверка формата данных для полей с датой и временем запуска рассылки и окончания рассылки
    @validator("start_date_time", pre=True, allow_reuse=True)
    @validator("end_date_time", pre=True, allow_reuse=True)
    def parse_date_time(cls, value):
        return datetime.datetime.fromtimestamp(int(value))


# Функция добавления нового клиента
@app.post("/client")
async def add_client(client: Client):
    db = SessionLocal()
    new_user = DbClient(
        id=client.id,
        phone_number=client.phone_number,
        operator_code=client.operator_code,
        tag=client.tag,
        timezone=client.timezone,
    )
    db.add(new_user)
    try:
        db.commit()
        db.refresh(new_user)
    except exc.IntegrityError:
        raise HTTPException(status_code=400, detail="Client already exists")
    return {"status": "Success"}


# Функция обновление данных атрибутов клиента
@app.put("/client/{client_id}")
async def update_client(client: Client, client_id: int):
    db = SessionLocal()
    if db.query(DbClient).get(client_id) is None:
        raise HTTPException(status_code=400, detail="Client not found")
    db.query(DbClient).get(client_id).id = client.id
    db.query(DbClient).get(client_id).phone_number = client.phone_number
    db.query(DbClient).get(client_id).operator_code = client.operator_code
    db.query(DbClient).get(client_id).tag = client.tag
    db.query(DbClient).get(client_id).timezone = client.timezone
    try:
        db.commit()
    except exc.IntegrityError:
        raise HTTPException(status_code=400, detail="Client with new id already exists")
    return {"status": "Success"}


# Функция удаления клиента из справочника
@app.delete("/client/{client_id}")
async def delete_client(client_id: int):
    db = SessionLocal()
    db.execute(delete(DbClient).where(DbClient.id == client_id))
    db.commit()
    return {"status": "Success"}


# Функция добавления новой рассылки со всеми её атрибутами
@app.post("/mailing")
async def add_mailing(mailing: Mailing):
    db = SessionLocal()
    new_mailing = DbMailing(
        id=mailing.id,
        start_date_time=mailing.start_date_time,
        end_date_time=mailing.end_date_time,
        text=mailing.text,
        filter=mailing.filter,

    )
    db.add(new_mailing)
    try:
        db.commit()
        db.refresh(new_mailing)
    except exc.IntegrityError:
        raise HTTPException(status_code=400, detail="Mailing already exists")
    get_filter = mailing.filter

    # Если текущее время больше времени начала и меньше времени окончания рассылки,
    # то будут выбраны из справочника все клиенты, которые подходят под значения фильтра, указанного в этой рассылке
    # и запущена отправка для всех этих клиентов
    if mailing.start_date_time.timestamp() <= datetime.datetime.now().timestamp() <= mailing.end_date_time.timestamp():
        client_list = [(mailing.id, mailing.text, client.id, client.phone_number) for client in
                       db.query(DbClient).filter(or_(DbClient.operator_code == get_filter, DbClient.tag == get_filter))]
        send_message.apply_async(args=[client_list])
    # Если создаётся рассылка с временем старта в будущем, то создается задача в Celery, с отложенном запуском
    elif mailing.start_date_time.timestamp() > datetime.datetime.now().timestamp():
        client_list = [(mailing.id, mailing.text, client.id, client.phone_number) for client in
                       db.query(DbClient).filter(or_(DbClient.operator_code == get_filter, DbClient.tag == get_filter))]
        send_message.apply_async(countdown=mailing.start_date_time.timestamp() - datetime.datetime.now().timestamp(),
                                 args=[client_list])
    return {"status": "Success"}


# Функция обновления атрибутов рассылки
@app.put("/mailing/{mailing_id}")
async def update_mailing(mailing: Mailing, mailing_id: int):
    db = SessionLocal()
    if db.query(DbMailing).get(mailing_id) is None:
        raise HTTPException(status_code=400, detail="Mailing not found")
    db.query(DbMailing).get(mailing_id).id = mailing.id
    db.query(DbMailing).get(mailing_id).start_date_time = mailing.start_date_time
    db.query(DbMailing).get(mailing_id).end_date_time = mailing.end_date_time
    db.query(DbMailing).get(mailing_id).text = mailing.text
    db.query(DbMailing).get(mailing_id).filter = mailing.filter
    db.commit()
    return {"status": "Success"}


# Функция удаления рассылки
@app.delete("/mailing/{mailing_id}")
async def delete_mailing(mailing_id: int):
    db = SessionLocal()
    db.execute(delete(DbMailing).where(DbMailing.id == mailing_id))
    db.commit()
    return {"status": "Success"}


# Функция получения общей статистики по созданным рассылкам
@app.get("/mailing/statistic/status")
async def status_stats_mailing():
    db = SessionLocal()
    result = []
    for message in db.query(DbMessage.status, func.count(DbMessage.id)).group_by(DbMessage.status).all():
        result.append({"status": message[0], "count": message[1]})
    return JSONResponse(content=result)


# Функция получения детальной статистики отправленных сообщений по конкретной рассылке
@app.get("/mailing/statistic/{mailing_id}")
async def stats_mailing(mailing_id: int):
    db = SessionLocal()
    result = []
    for message in db.query(DbMessage).filter(DbMessage.mailing_id == mailing_id):
        result.append(
            {"create_date_time": message.create_date_time.strftime("%d-%m-%Y %H:%M:%S"), "status": message.status})
    return JSONResponse(content=result)


# Функция обработки активных рассылок и отправки сообщений клиентам
@app.get("/mailing/start")
async def start_mailing():
    db = SessionLocal()
    now_datetime = datetime.datetime.now()
    client_list = []
    for mailing in db.query(DbMailing).filter(
            DbMailing.end_date_time > now_datetime >= DbMailing.start_date_time).all():
        client_list += [(mailing.id, mailing.text, client.id, client.phone_number) for client in
                        db.query(DbClient).filter(
                            or_(DbClient.operator_code == mailing.filter, DbClient.tag == mailing.filter))]
    send_message.apply_async(args=[client_list])
    return {"status": "Success"}
